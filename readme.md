# Raspberry Pi Automated k3s Install
The included playbooks will allow you to prep, install and setup k3s on Raspberry Pis. 
Some prerequists do exist: 
* Make sure Ansible is installed on your machine
* Have at least 2 Raspberry Pis. (Hard to find?) Check here https://rpilocator.com/
* Install Raspbian or Ubuntu as a headless install
* Edit the cmdline.txt file to include the following
```
group_memory=1 cgroup_enable=memory
```
To set a static ip address by using the following line in cmdline.txt
```
ip=192.168.0.201::192.168.1.1:255.255.255.0:rpiname:eth0:off
```
Make sure `arm_64bit=1` is set in config.txt


If you are using a username & password to initially get into the Raspberry Pis make sure this is installed:

On MacOS: 
`brew install hudochenkov/sshpass/sshpass`

On Linux: 
`apt-get install sshpass`

On Windows: 
`Install Linux or buy a Mac then see above`

## Set up inventory
Edit `./hosts` in this repo with your username and password and Raspberry Pi ip address be sure to separate the master from the worker nodes. 

`ansible_user` = Your Raspberry Pi's user default is `pi`

`ansible_ssh_pass` = You Raspberry Pi's password default is `raspberry`

`all-raspberry-pis` = Include all Raspberry Pis here

`master` = Only include the master Raspberry Pi

`workers` = Only include the worker Raspberry Pis



## Running the playbooks

Prep all Pis for k3s
```
ansible-playbook prep-all-pis.yaml -i hosts
```

Install k3s
```
ansible-playbook install-k3s.yaml -i hosts
```


Copy over kubeconfig 
scp k3s-user@192.168.0.201:/etc/rancher/k3s/k3s.yaml ~/.kube/config



download cert-manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases

```


```
kubectl apply -f ./trefik-config/letsencrypt-issuer.yaml 